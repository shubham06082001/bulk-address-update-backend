const express = require("express")
const mongoose = require("mongoose")
const multer = require("multer")
const csv = require("csv-parser")
const fs = require("fs")
const Redis = require("ioredis")
const cors = require("cors")
const { Readable } = require("stream")

const app = express()
const port = 3000

app.use(cors())

// MongoDB connection setup
const connectDB = async () => {
  const conn = await mongoose.connect(
    "mongodb+srv://shubhamkmr06082001:jl6RXqidtv3Eg1ET@database.mk1kzpb.mongodb.net/bulk-update-legacy",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  console.log(`MongoDB connected : ${conn.connection.host}`)
}

connectDB()

// Create a mongoose model for addresses
const Address = mongoose.model("Address", {
  name: String,
  street: String,
  city: String,
  state: String,
  zip: String,
})

// Set up Multer for file upload
const storage = multer.memoryStorage()
const upload = multer({
  storage: storage,
  limits: { fileSize: 5 * 1024 * 1024 }, // 5MB limit
})

// Middleware to parse JSON requests
app.use(express.json())

// Redis setup
const redis = new Redis()

// SCENARIO 2
app.post(
  "/api/bulk-update-and-cache",
  upload.single("file"),
  async (req, res) => {
    const fileBuffer = req.file.buffer
    const addresses = []

    await Address.deleteMany({})

    const readableStream = new Readable()
    readableStream.push(fileBuffer)
    readableStream.push(null)

    readableStream
      .pipe(csv())
      .on("data", (row) => {
        const address = new Address({
          name: row.name,
          street: row.street,
          city: row.city,
          state: row.state,
          zip: row.zip,
        })
        addresses.push(address)
      })
      .on("end", async () => {
        try {
          await Address.insertMany(addresses)
          const cacheKey = "bulk-addresses"
          await redis.set(cacheKey, JSON.stringify(addresses))
          res
            .status(201)
            .json({ message: "Bulk address update and caching complete" })
        } catch (error) {
          console.error("Error during bulk address update:", error)
          res.status(500).json({ error: "Error during bulk address update" })
        }
      })
  }
)

// SCENARIO 2
app.post("/api/update-address", async (req, res) => {
  try {
    const data = req.body
    console.log(data);
    let addresses
    await Address.deleteMany({})
    if (Array.isArray(data)) {
      addresses = data.map((addressData) => new Address(addressData))
      await Address.insertMany(addresses)
    } else {
      addresses = new Address(data)
      await addresses.save()
    }
    const cacheKey = "bulk-addresses"
    await redis.set(cacheKey, JSON.stringify(addresses))

    res.status(201).json({ message: "Address updated successfully" })
  } catch (error) {
    console.error("Error during address update:", error)
    res.status(500).json({ error: "Error during address update" })
  }
})

const cacheKey = "bulk-addresses"

// SCENARIO 1
app.get("/api/mongo-addresses", async (req, res) => {
  try {
    const addresses = await Address.find().exec()
    await redis.set(cacheKey, JSON.stringify(addresses))
    res.json({ addresses, source: "MongoDB" })
  } catch (error) {
    console.error("Error during address retrieval:", error)
    res.status(500).json({ error: "Error during address retrieval" })
  }
})


// SCENARIO 1

app.get("/api/cached-addresses", async (req, res) => {
  const cacheKey = "bulk-addresses"
  const cachedData = await redis.get(cacheKey)

  if (cachedData) {
    const currentTime = new Date().toLocaleTimeString()
    console.log("Cached data refreshed at:", currentTime)
    const addresses = JSON.parse(cachedData)
    res.status(200).json({ addresses })
  } else {
    res.status(404).json({ message: "Cached data not found" })
  }
})

// SCENARIO 3
app.get("/api/clear-all-cached-data", (req, res) => {
  redis.flushall((err, reply) => {
    if (err) {
      console.error("Error clearing all cached data:", err)
      res.status(500).json({ message: "Error clearing all cached data" })
    } else {
      console.log("All cached data cleared")
      res.status(200).json({ message: "All cached data cleared" })
    }
  })
})

// SCENARIO 4
app.get("/api/clear-all-mongodb-data", async (req, res) => {
  try {
    await Address.deleteMany({})
    const cacheKey = "bulk-addresses"
    redis.del(cacheKey, (err, reply) => {
      if (err) {
        res.status(200).json({ message: "All MongoDB and Redis data cleared" })
      } else {
        res.status(200).json({ message: "All MongoDB data cleared" })
      }
    })
  } catch (error) {
    console.error("Error clearing all MongoDB data:", error)
    res.status(500).json({ message: "Error clearing all MongoDB data" })
  }
})

// SCENARIO 5
app.get("/api/refresh-cached-data", (req, res) => {
  const freshData = [
    {
      name: "New John Doe",
      street: "456 Oak St",
      city: "Miami",
      state: "FL",
      zip: "33102",
    },
  ]

  const cacheKey = "bulk-addresses"

  redis.del(cacheKey, (err, reply) => {
    if (err) {
      console.error("Error clearing cached data:", err)
      res.status(500).json({ message: "Error clearing cached data" })
    } else {
      redis.set(cacheKey, JSON.stringify(freshData), (err) => {
        if (err) {
          console.error("Error caching refreshed data:", err)
          res.status(500).json({ message: "Error caching refreshed data" })
        } else {
          const currentTime = new Date().toLocaleTimeString()
          console.log("Cached data refreshed at:", currentTime)
          res.status(200).json({ message: "Cached data refreshed" })
        }
      })
    }
  })
})

app.listen(port, () => {
  console.log(`Server is running on port ${port}`)
})
